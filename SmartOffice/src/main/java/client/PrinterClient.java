/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import clientui.PrinterGUI;
import com.google.protobuf.Empty;
import com.office.printer.PrinContent;
import com.office.printer.PrintStatus;
import io.grpc.ManagedChannelBuilder;
import com.office.printer.PrinterGrpc;
import io.grpc.ManagedChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author princesstamagatchi
 */
public class PrinterClient implements ServiceObserver{
    
    protected PrinterGUI ui;
    private ManagedChannel channel;
    PrinterGrpc.PrinterBlockingStub printerStub;
    protected ServiceDescription current;
    private final String serviceType;
    private final String name;
    private static final Logger logger = Logger.getLogger(PrinterClient.class.getName());
    
    public PrinterClient() {
        serviceType = "_printer._udp.local.";
        name = "Office";
        jmDNSServiceTracker clientManager = jmDNSServiceTracker.getInstance();
        clientManager.register(this);
         java.awt.EventQueue.invokeLater(new Runnable() {
             public void run() {
                 ui = new PrinterGUI(PrinterClient.this);
                 ui.setVisible(true);
             }
         });
        
         
        serviceAdded(new ServiceDescription("localhost", 50024));
    }
    
    public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }
    
    

    public List<String> serviceInterests() {
        List<String> interests = new ArrayList<String>();
        interests.add(serviceType);
        return interests;
    }
    
    public boolean interested(String type) {
        return serviceType.equals(type);
    }

    public void serviceAdded(ServiceDescription service) {
        System.out.println("Printer service added");
        current = service;
        channel = ManagedChannelBuilder.forAddress(service.getAddress(), service.getPort())
                .usePlaintext(true)
                .build();
        printerStub = PrinterGrpc.newBlockingStub(channel);
    }
    

    public String getName() {
        return name;
    }

    public void switchService(String name) {
        // TODO
        
    }
    
    public void toggle() {
        
        try {
            Empty request = Empty.newBuilder().build();
            com.office.printer.Response rsp = printerStub.toggle(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
        
    }
    
     public void print() {
        
         
         try {

            new Thread() {
                public void run() {
                    com.office.printer.PrinContent content = PrinContent.newBuilder().setContent("document").build();
                    Iterator<PrintStatus> rsp = printerStub.print(content);
                    
                    while (rsp.hasNext()) {
                        ui.append(rsp.next().toString());
                    }
                }
            }.start();
            
            Empty request = Empty.newBuilder().build();
            PrintStatus status = printerStub.printCompletion(request);
            ui.append(status.toString());

        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
         
         
         
         
         
//        try {
//            com.office.printer.PrinContent content = PrinContent.newBuilder().setContent("document").build();
//            Iterator<PrintStatus> rsp = printerStub.print(content);
//            System.out.println(rsp);
//            ui.append(rsp.toString());
//        } catch (RuntimeException e) {
//            logger.log(Level.WARNING, "RPC failed", e);
//        }
        
    }
     
      public void status() {
        
        try {
            Empty request = Empty.newBuilder().build();
            com.office.printer.Response rsp = printerStub.status(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
        
    }
    
    
    
    
    public static void main(String[] args) throws Exception {
        new PrinterClient();
    }

}

