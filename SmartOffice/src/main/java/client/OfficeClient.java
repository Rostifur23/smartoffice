/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import clientui.ClientGUI;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannelBuilder;
import com.office.lights.LightsGrpc;
import com.office.lights.Response;
import io.grpc.ManagedChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author princesstamagatchi
 */
public class OfficeClient implements ServiceObserver{
    
    protected ClientGUI ui;
    private ManagedChannel channel;
    private LightsGrpc.LightsBlockingStub lightsStub;
    protected ServiceDescription current;
    private final String serviceType;
    private final String name;
    private static final Logger logger = Logger.getLogger(OfficeClient.class.getName());
    
    public OfficeClient() {
        serviceType = "_lights._udp.local.";
        name = "Office";
        jmDNSServiceTracker clientManager = jmDNSServiceTracker.getInstance();
        clientManager.register(this);
         java.awt.EventQueue.invokeLater(new Runnable() {
             public void run() {
                 ui = new ClientGUI(OfficeClient.this);
                 ui.setVisible(true);
             }
         });
        
         
        serviceAdded(new ServiceDescription("localhost", 50021));
    }
    
    public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }
    
    

    public List<String> serviceInterests() {
        List<String> interests = new ArrayList<String>();
        interests.add(serviceType);
        return interests;
    }
    
    public boolean interested(String type) {
        return serviceType.equals(type);
    }

    public void serviceAdded(ServiceDescription service) {
        System.out.println("Lights service added");
        current = service;
        channel = ManagedChannelBuilder.forAddress(service.getAddress(), service.getPort())
                .usePlaintext(true)
                .build();
        lightsStub = LightsGrpc.newBlockingStub(channel);
    }
    

    public String getName() {
        return name;
    }

    public void switchService(String name) {
        // TODO
        
    }
    
    public void toggle() {
        
        try {
            Empty request = Empty.newBuilder().build();
            Response rsp = lightsStub.toggle(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
        
    }
    
    public void lightStatus(){
        try {
            Empty request = Empty.newBuilder().build();
            Response rsp = lightsStub.status(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
    }
    
    public void dimmer(){
        try {
            Empty request = Empty.newBuilder().build();
            Response rsp = lightsStub.dimmer(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
    }
    
    public void brighter(){
        try {
            Empty request = Empty.newBuilder().build();
            Response rsp = lightsStub.brighter(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
    }
    
    
    public static void main(String[] args) throws Exception {
        OfficeClient client = new OfficeClient();
    }

}
