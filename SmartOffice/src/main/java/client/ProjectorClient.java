/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import clientui.ProjectorGUI;
import com.google.protobuf.Empty;
import com.office.projector.ProjectorGrpc;
import com.office.temp.TempGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author princesstamagatchi
 */
public class ProjectorClient implements ServiceObserver {

    protected ProjectorGUI ui;
    private ManagedChannel channel;
    private ProjectorGrpc.ProjectorBlockingStub projectorStub;
    protected ServiceDescription current;
    private final String serviceType;
    private final String name;
    private static final Logger logger = Logger.getLogger(ProjectorClient.class.getName());
    
    public ProjectorClient() {
        serviceType = "_projector._udp.local.";
        name = "Office";
        jmDNSServiceTracker clientManager = jmDNSServiceTracker.getInstance();
        clientManager.register(this);
         java.awt.EventQueue.invokeLater(new Runnable() {
             public void run() {
                 ui = new ProjectorGUI(ProjectorClient.this);
                 ui.setVisible(true);
             }
         });
        
         
        serviceAdded(new ServiceDescription("localhost", 50023));
    }
    
    public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }
    
    

    public List<String> serviceInterests() {
        List<String> interests = new ArrayList<String>();
        interests.add(serviceType);
        return interests;
    }
    
    public boolean interested(String type) {
        return serviceType.equals(type);
    }

    public void serviceAdded(ServiceDescription service) {
        System.out.println("Projector service added");
        current = service;
        channel = ManagedChannelBuilder.forAddress(service.getAddress(), service.getPort())
                .usePlaintext(true)
                .build();
        projectorStub = ProjectorGrpc.newBlockingStub(channel);
    }  

    public String getName() {
        return name;
    }

    public void switchService(String name) {
        // TODO
        
    }
    
    public void toggle() {
        
        try {
            Empty request = Empty.newBuilder().build();
            com.office.projector.Response rsp = projectorStub.toggle(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
        
    }
    
    public void currentDisplay() {
        
        try {
            Empty request = Empty.newBuilder().build();
            com.office.projector.Response rsp = projectorStub.currentDisplay(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
        
    }
    
    public void switchDisplay() {
        
        try {
            Empty request = Empty.newBuilder().build();
            com.office.projector.Response rsp = projectorStub.switchDisplay(request);
            System.out.println(rsp);
            ui.append(rsp.toString());
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "RPC failed", e);
        }
        
    }
    
      
    
    
    public static void main(String[] args) throws Exception {
        new ProjectorClient();
    }
    
}
