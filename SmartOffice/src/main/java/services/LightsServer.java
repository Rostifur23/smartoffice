/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.office.lights.LightsGrpc;
import com.office.lights.Response;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.util.logging.Logger;

/**
 *
 * @author princesstamagatchi
 */
public class LightsServer {
    private static final Logger logger = Logger.getLogger(LightsServer.class.getName());

    /* The port on which the server should run */
    private int port = 50021;
    private Server server;

    public void start() throws Exception {
        server = ServerBuilder.forPort(port)
                .addService(new LightsServer.LightsImpl())
                .build()
                .start();
        JmDNSRegistrationHelper helper = new JmDNSRegistrationHelper("Office", "_lights._udp.local.", "", port);
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                LightsServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon
     * threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Main launches the server from the command line.
     */
    public static void main(String[] args) throws Exception {
        final LightsServer server = new LightsServer();
        server.start();
        server.blockUntilShutdown();
    }
    
    
    
    
    
    
    
    
    public class LightsImpl extends LightsGrpc.LightsImplBase {

        public LightsImpl() {
            String name = "Office";
            String serviceType = "_lights._udp.local.";
        }
        
        private int brightness = 0;

        @Override
        public void toggle(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            if(brightness == 0) {
                responseObserver.onNext(Response.newBuilder().setResponse("Lights have turned on").build());
                responseObserver.onCompleted();
                brightness = 10;
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Lights have turned off").build());
                responseObserver.onCompleted();
                brightness = 0;
            }
            
        }
        
        @Override
        public void status(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            if(brightness == 0) {
                responseObserver.onNext(Response.newBuilder().setResponse("Lights are turned off").build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Lights are at brightness level "+brightness).build());
                responseObserver.onCompleted();
            }

        }
        
        @Override
        public void brighter(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            if(brightness < 10) {
                brightness = brightness+1;
                responseObserver.onNext(Response.newBuilder().setResponse("Lights adjusted to brightness level "+brightness).build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Lights are at maximum brightness level").build());
                responseObserver.onCompleted();
            }
            

        }
        
        @Override
        public void dimmer(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            if(brightness != 0) {
                brightness = brightness-1;
                responseObserver.onNext(Response.newBuilder().setResponse("Lights have dimmed to brightness level "+brightness).build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Lights can not be dimmed").build());
                responseObserver.onCompleted();
            }
            
        }

        

    }
}
