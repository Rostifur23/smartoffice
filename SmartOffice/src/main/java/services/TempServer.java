/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.office.temp.Response;
import com.office.temp.TempGrpc;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.util.logging.Logger;



/**
 *
 * @author princesstamagatchi
 */
public class TempServer {
    private static final Logger logger = Logger.getLogger(TempServer.class.getName());

    /* The port on which the server should run */
    private int port = 50022;
    private Server server;

    public void start() throws Exception {
        server = ServerBuilder.forPort(port)
                .addService(new TempServer.TempImpl())
                .build()
                .start();
        JmDNSRegistrationHelper helper = new JmDNSRegistrationHelper("Office", "_temp._udp.local.", "", port);
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                TempServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon
     * threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Main launches the server from the command line.
     */
    public static void main(String[] args) throws Exception {
        final TempServer server = new TempServer();
        server.start();
        server.blockUntilShutdown();
    }
    
    
    
    
    
    
    
    
    public class TempImpl extends TempGrpc.TempImplBase {

        public TempImpl() {
            String name = "Office";
            String serviceType = "_temp._udp.local.";
        }
        
        private int temp = 23;

        @Override
        public void get(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            responseObserver.onNext(Response.newBuilder().setResponse("Current Temperature: "+ temp).build());
            responseObserver.onCompleted();
            
        }
        
        @Override
        public void higher(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            if(temp < 36){
                temp = temp+1;
                responseObserver.onNext(Response.newBuilder().setResponse("Current Temperature Increased to "+temp).build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Current Temperature at Maximum").build());
                responseObserver.onCompleted();
            }

        }
        
        @Override
        public void lower(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            
            if(temp > 0){
                temp = temp-1;
                responseObserver.onNext(Response.newBuilder().setResponse("Current Temperature Decreased to "+temp).build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Current Temperature at Minimum").build());
                responseObserver.onCompleted();
            }

        }
        
        

        

    }
}
