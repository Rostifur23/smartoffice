/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.office.projector.Response;
import com.office.projector.ProjectorGrpc;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.util.logging.Logger;



/**
 *
 * @author princesstamagatchi
 */
public class ProjectorServer {
    private static final Logger logger = Logger.getLogger(ProjectorServer.class.getName());

    /* The port on which the server should run */
    private int port = 50023;
    private Server server;

    public void start() throws Exception {
        server = ServerBuilder.forPort(port)
                .addService(new ProjectorServer.ProjectorImpl())
                .build()
                .start();
        JmDNSRegistrationHelper helper = new JmDNSRegistrationHelper("Office", "_projector._udp.local.", "", port);
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                ProjectorServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon
     * threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Main launches the server from the command line.
     */
    public static void main(String[] args) throws Exception {
        final ProjectorServer server = new ProjectorServer();
        server.start();
        server.blockUntilShutdown();
    }
    
    
    
    
    
    
    
    
    public class ProjectorImpl extends ProjectorGrpc.ProjectorImplBase {

        public ProjectorImpl() {
            String name = "Office";
            String serviceType = "_projector._udp.local.";
        }
        
        private boolean projectorOn = false;
        private String display = "none";
        

        @Override
        public void toggle(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            if(projectorOn){
                responseObserver.onNext(Response.newBuilder().setResponse("Projector is now offline").build());
                responseObserver.onCompleted();
                projectorOn = false;
                display = "Powered off";
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Projector is now online").build());
                responseObserver.onCompleted();
                projectorOn = true;
                display = "HDMI";
            }
            
        }
        
        
        
        @Override
        public void switchDisplay(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<com.office.projector.Response> responseObserver) {
            if(display.equals("HDMI")){
                display = "VGA";
                responseObserver.onNext(Response.newBuilder().setResponse("Display switched to "+display).build());
                responseObserver.onCompleted();
            } else if(display.equals("VGA")){
                display = "HDMI";
                responseObserver.onNext(Response.newBuilder().setResponse("Display switched to "+display).build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Projector is powered off").build());
                responseObserver.onCompleted();
            }
            
        }  
        
        @Override
        public void currentDisplay(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<com.office.projector.Response> responseObserver) {
                
                responseObserver.onNext(Response.newBuilder().setResponse("Current Display: "+display).build());
                responseObserver.onCompleted();
            
        }  

    }
}
