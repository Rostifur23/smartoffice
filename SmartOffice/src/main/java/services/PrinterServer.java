/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.office.printer.PrintStatus;
import com.office.printer.Response;
import com.office.printer.PrinterGrpc;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;



/**
 *
 * @author princesstamagatchi
 */
public class PrinterServer {
    private static final Logger logger = Logger.getLogger(PrinterServer.class.getName());

    /* The port on which the server should run */
    private int port = 50024;
    private Server server;

    public void start() throws Exception {
        server = ServerBuilder.forPort(port)
                .addService(new PrinterServer.PrinterImpl())
                .build()
                .start();
        JmDNSRegistrationHelper helper = new JmDNSRegistrationHelper("Office", "_printer._udp.local.", "", port);
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                PrinterServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon
     * threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Main launches the server from the command line.
     */
    public static void main(String[] args) throws Exception {
        final PrinterServer server = new PrinterServer();
        server.start();
        server.blockUntilShutdown();
    }
    
    
    public class PrinterImpl extends PrinterGrpc.PrinterImplBase {

        public PrinterImpl() {
            String name = "Office";
            String serviceType = "_printer._udp.local.";
        }
        
        private int percentPrinted = 0;
        private boolean printerOn = false;
        

        @Override
        public void toggle(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<Response> responseObserver) {
            
            if(printerOn){
                responseObserver.onNext(Response.newBuilder().setResponse("Printer is now offline").build());
                responseObserver.onCompleted();
                printerOn = false;
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Printer is now online").build());
                responseObserver.onCompleted();
                printerOn = true;
            }
            
        }
        
        
        
        @Override
        public void print(com.office.printer.PrinContent request,
                io.grpc.stub.StreamObserver<com.office.printer.PrintStatus> responseObserver) {
            
            if(!printerOn){
                System.out.println("Powering on Printer");
                printerOn = true;
            }
            
            Timer t = new Timer();
            t.schedule(new RemindTask(responseObserver), 0, 2000);
            
           
            
        }
        
        @Override
        public void status(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<com.office.printer.Response> responseObserver) {
            
            if(printerOn){
                responseObserver.onNext(Response.newBuilder().setResponse("Printer is currently powered on").build());
                responseObserver.onCompleted();
            } else {
                responseObserver.onNext(Response.newBuilder().setResponse("Printer is currently powered off").build());
                responseObserver.onCompleted();
            }
            

        }
        
        @Override
        public void printCompletion(com.google.protobuf.Empty request,
                io.grpc.stub.StreamObserver<com.office.printer.PrintStatus> responseObserver) {
            responseObserver.onNext(PrintStatus.newBuilder().setCompleted(percentPrinted).build());
            responseObserver.onCompleted();
        }
        
        class RemindTask extends TimerTask {

            StreamObserver<PrintStatus> o;

            public RemindTask(StreamObserver<PrintStatus> j) {
                o = j;
            }

            @Override
            public void run() {
                if (percentPrinted < 100) {
                    percentPrinted += 10;
                    PrintStatus status = PrintStatus.newBuilder().setCompleted(percentPrinted).build();
                    o.onNext(status);
                } else {
                    o.onCompleted();
                    this.cancel();
                }
            }
        }
        
        

        

    }
}
