
import client.OfficeClient;
import client.PrinterClient;
import client.ProjectorClient;
import client.TempClient;
import services.LightsServer;
import services.PrinterServer;
import services.ProjectorServer;
import services.TempServer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author princesstamagatchi
 */
public class SmartOffice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        LightsServer light = new LightsServer();
        TempServer temp = new TempServer();
        PrinterServer printer = new PrinterServer();
        ProjectorServer projector = new ProjectorServer();
        
        
        
        light.start();
        temp.start();
        printer.start();
        projector.start();
        
        OfficeClient lightC = new OfficeClient();
        PrinterClient printC = new PrinterClient();
        ProjectorClient projectorC = new ProjectorClient();
        TempClient tempC = new TempClient();
        
        
        
    }
    
}
