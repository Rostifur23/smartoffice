// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: printer.proto

package com.office.printer;

public interface ResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:printer.Response)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string response = 1;</code>
   */
  java.lang.String getResponse();
  /**
   * <code>string response = 1;</code>
   */
  com.google.protobuf.ByteString
      getResponseBytes();
}
