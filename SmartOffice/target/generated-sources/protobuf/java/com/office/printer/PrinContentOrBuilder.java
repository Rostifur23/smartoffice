// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: printer.proto

package com.office.printer;

public interface PrinContentOrBuilder extends
    // @@protoc_insertion_point(interface_extends:printer.PrinContent)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string content = 1;</code>
   */
  java.lang.String getContent();
  /**
   * <code>string content = 1;</code>
   */
  com.google.protobuf.ByteString
      getContentBytes();
}
