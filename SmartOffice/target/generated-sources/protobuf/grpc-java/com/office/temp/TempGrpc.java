package com.office.temp;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The temperature service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.19.0)",
    comments = "Source: temp.proto")
public final class TempGrpc {

  private TempGrpc() {}

  public static final String SERVICE_NAME = "temp.Temp";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.temp.Response> getGetMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Get",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.temp.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.temp.Response> getGetMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.temp.Response> getGetMethod;
    if ((getGetMethod = TempGrpc.getGetMethod) == null) {
      synchronized (TempGrpc.class) {
        if ((getGetMethod = TempGrpc.getGetMethod) == null) {
          TempGrpc.getGetMethod = getGetMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.temp.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "temp.Temp", "Get"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.temp.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new TempMethodDescriptorSupplier("Get"))
                  .build();
          }
        }
     }
     return getGetMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.temp.Response> getHigherMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Higher",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.temp.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.temp.Response> getHigherMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.temp.Response> getHigherMethod;
    if ((getHigherMethod = TempGrpc.getHigherMethod) == null) {
      synchronized (TempGrpc.class) {
        if ((getHigherMethod = TempGrpc.getHigherMethod) == null) {
          TempGrpc.getHigherMethod = getHigherMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.temp.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "temp.Temp", "Higher"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.temp.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new TempMethodDescriptorSupplier("Higher"))
                  .build();
          }
        }
     }
     return getHigherMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.temp.Response> getLowerMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Lower",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.temp.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.temp.Response> getLowerMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.temp.Response> getLowerMethod;
    if ((getLowerMethod = TempGrpc.getLowerMethod) == null) {
      synchronized (TempGrpc.class) {
        if ((getLowerMethod = TempGrpc.getLowerMethod) == null) {
          TempGrpc.getLowerMethod = getLowerMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.temp.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "temp.Temp", "Lower"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.temp.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new TempMethodDescriptorSupplier("Lower"))
                  .build();
          }
        }
     }
     return getLowerMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static TempStub newStub(io.grpc.Channel channel) {
    return new TempStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static TempBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new TempBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static TempFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new TempFutureStub(channel);
  }

  /**
   * <pre>
   * The temperature service definition.
   * </pre>
   */
  public static abstract class TempImplBase implements io.grpc.BindableService {

    /**
     */
    public void get(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.temp.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getGetMethod(), responseObserver);
    }

    /**
     */
    public void higher(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.temp.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getHigherMethod(), responseObserver);
    }

    /**
     */
    public void lower(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.temp.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getLowerMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.temp.Response>(
                  this, METHODID_GET)))
          .addMethod(
            getHigherMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.temp.Response>(
                  this, METHODID_HIGHER)))
          .addMethod(
            getLowerMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.temp.Response>(
                  this, METHODID_LOWER)))
          .build();
    }
  }

  /**
   * <pre>
   * The temperature service definition.
   * </pre>
   */
  public static final class TempStub extends io.grpc.stub.AbstractStub<TempStub> {
    private TempStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TempStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TempStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TempStub(channel, callOptions);
    }

    /**
     */
    public void get(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.temp.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void higher(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.temp.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getHigherMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void lower(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.temp.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLowerMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The temperature service definition.
   * </pre>
   */
  public static final class TempBlockingStub extends io.grpc.stub.AbstractStub<TempBlockingStub> {
    private TempBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TempBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TempBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TempBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.office.temp.Response get(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getGetMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.office.temp.Response higher(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getHigherMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.office.temp.Response lower(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getLowerMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The temperature service definition.
   * </pre>
   */
  public static final class TempFutureStub extends io.grpc.stub.AbstractStub<TempFutureStub> {
    private TempFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private TempFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TempFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new TempFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.temp.Response> get(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getGetMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.temp.Response> higher(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getHigherMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.temp.Response> lower(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getLowerMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET = 0;
  private static final int METHODID_HIGHER = 1;
  private static final int METHODID_LOWER = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final TempImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(TempImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET:
          serviceImpl.get((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.temp.Response>) responseObserver);
          break;
        case METHODID_HIGHER:
          serviceImpl.higher((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.temp.Response>) responseObserver);
          break;
        case METHODID_LOWER:
          serviceImpl.lower((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.temp.Response>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class TempBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    TempBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.office.temp.TempProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Temp");
    }
  }

  private static final class TempFileDescriptorSupplier
      extends TempBaseDescriptorSupplier {
    TempFileDescriptorSupplier() {}
  }

  private static final class TempMethodDescriptorSupplier
      extends TempBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    TempMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (TempGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new TempFileDescriptorSupplier())
              .addMethod(getGetMethod())
              .addMethod(getHigherMethod())
              .addMethod(getLowerMethod())
              .build();
        }
      }
    }
    return result;
  }
}
