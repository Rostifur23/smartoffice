package com.office.printer;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The printer service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.19.0)",
    comments = "Source: printer.proto")
public final class PrinterGrpc {

  private PrinterGrpc() {}

  public static final String SERVICE_NAME = "printer.Printer";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.printer.Response> getToggleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Toggle",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.printer.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.printer.Response> getToggleMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.printer.Response> getToggleMethod;
    if ((getToggleMethod = PrinterGrpc.getToggleMethod) == null) {
      synchronized (PrinterGrpc.class) {
        if ((getToggleMethod = PrinterGrpc.getToggleMethod) == null) {
          PrinterGrpc.getToggleMethod = getToggleMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.printer.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "printer.Printer", "Toggle"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.printer.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new PrinterMethodDescriptorSupplier("Toggle"))
                  .build();
          }
        }
     }
     return getToggleMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.office.printer.PrinContent,
      com.office.printer.PrintStatus> getPrintMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Print",
      requestType = com.office.printer.PrinContent.class,
      responseType = com.office.printer.PrintStatus.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.office.printer.PrinContent,
      com.office.printer.PrintStatus> getPrintMethod() {
    io.grpc.MethodDescriptor<com.office.printer.PrinContent, com.office.printer.PrintStatus> getPrintMethod;
    if ((getPrintMethod = PrinterGrpc.getPrintMethod) == null) {
      synchronized (PrinterGrpc.class) {
        if ((getPrintMethod = PrinterGrpc.getPrintMethod) == null) {
          PrinterGrpc.getPrintMethod = getPrintMethod = 
              io.grpc.MethodDescriptor.<com.office.printer.PrinContent, com.office.printer.PrintStatus>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "printer.Printer", "Print"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.printer.PrinContent.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.printer.PrintStatus.getDefaultInstance()))
                  .setSchemaDescriptor(new PrinterMethodDescriptorSupplier("Print"))
                  .build();
          }
        }
     }
     return getPrintMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.printer.Response> getStatusMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Status",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.printer.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.printer.Response> getStatusMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.printer.Response> getStatusMethod;
    if ((getStatusMethod = PrinterGrpc.getStatusMethod) == null) {
      synchronized (PrinterGrpc.class) {
        if ((getStatusMethod = PrinterGrpc.getStatusMethod) == null) {
          PrinterGrpc.getStatusMethod = getStatusMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.printer.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "printer.Printer", "Status"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.printer.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new PrinterMethodDescriptorSupplier("Status"))
                  .build();
          }
        }
     }
     return getStatusMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.printer.PrintStatus> getPrintCompletionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "printCompletion",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.printer.PrintStatus.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.printer.PrintStatus> getPrintCompletionMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.printer.PrintStatus> getPrintCompletionMethod;
    if ((getPrintCompletionMethod = PrinterGrpc.getPrintCompletionMethod) == null) {
      synchronized (PrinterGrpc.class) {
        if ((getPrintCompletionMethod = PrinterGrpc.getPrintCompletionMethod) == null) {
          PrinterGrpc.getPrintCompletionMethod = getPrintCompletionMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.printer.PrintStatus>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "printer.Printer", "printCompletion"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.printer.PrintStatus.getDefaultInstance()))
                  .setSchemaDescriptor(new PrinterMethodDescriptorSupplier("printCompletion"))
                  .build();
          }
        }
     }
     return getPrintCompletionMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static PrinterStub newStub(io.grpc.Channel channel) {
    return new PrinterStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static PrinterBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new PrinterBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static PrinterFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new PrinterFutureStub(channel);
  }

  /**
   * <pre>
   * The printer service definition.
   * </pre>
   */
  public static abstract class PrinterImplBase implements io.grpc.BindableService {

    /**
     */
    public void toggle(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.printer.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getToggleMethod(), responseObserver);
    }

    /**
     */
    public void print(com.office.printer.PrinContent request,
        io.grpc.stub.StreamObserver<com.office.printer.PrintStatus> responseObserver) {
      asyncUnimplementedUnaryCall(getPrintMethod(), responseObserver);
    }

    /**
     */
    public void status(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.printer.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getStatusMethod(), responseObserver);
    }

    /**
     */
    public void printCompletion(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.printer.PrintStatus> responseObserver) {
      asyncUnimplementedUnaryCall(getPrintCompletionMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getToggleMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.printer.Response>(
                  this, METHODID_TOGGLE)))
          .addMethod(
            getPrintMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.office.printer.PrinContent,
                com.office.printer.PrintStatus>(
                  this, METHODID_PRINT)))
          .addMethod(
            getStatusMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.printer.Response>(
                  this, METHODID_STATUS)))
          .addMethod(
            getPrintCompletionMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.printer.PrintStatus>(
                  this, METHODID_PRINT_COMPLETION)))
          .build();
    }
  }

  /**
   * <pre>
   * The printer service definition.
   * </pre>
   */
  public static final class PrinterStub extends io.grpc.stub.AbstractStub<PrinterStub> {
    private PrinterStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrinterStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrinterStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrinterStub(channel, callOptions);
    }

    /**
     */
    public void toggle(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.printer.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getToggleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void print(com.office.printer.PrinContent request,
        io.grpc.stub.StreamObserver<com.office.printer.PrintStatus> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getPrintMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void status(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.printer.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getStatusMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void printCompletion(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.printer.PrintStatus> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getPrintCompletionMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The printer service definition.
   * </pre>
   */
  public static final class PrinterBlockingStub extends io.grpc.stub.AbstractStub<PrinterBlockingStub> {
    private PrinterBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrinterBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrinterBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrinterBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.office.printer.Response toggle(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getToggleMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<com.office.printer.PrintStatus> print(
        com.office.printer.PrinContent request) {
      return blockingServerStreamingCall(
          getChannel(), getPrintMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.office.printer.Response status(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getStatusMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.office.printer.PrintStatus printCompletion(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getPrintCompletionMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The printer service definition.
   * </pre>
   */
  public static final class PrinterFutureStub extends io.grpc.stub.AbstractStub<PrinterFutureStub> {
    private PrinterFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PrinterFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PrinterFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PrinterFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.printer.Response> toggle(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getToggleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.printer.Response> status(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getStatusMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.printer.PrintStatus> printCompletion(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getPrintCompletionMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_TOGGLE = 0;
  private static final int METHODID_PRINT = 1;
  private static final int METHODID_STATUS = 2;
  private static final int METHODID_PRINT_COMPLETION = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final PrinterImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(PrinterImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_TOGGLE:
          serviceImpl.toggle((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.printer.Response>) responseObserver);
          break;
        case METHODID_PRINT:
          serviceImpl.print((com.office.printer.PrinContent) request,
              (io.grpc.stub.StreamObserver<com.office.printer.PrintStatus>) responseObserver);
          break;
        case METHODID_STATUS:
          serviceImpl.status((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.printer.Response>) responseObserver);
          break;
        case METHODID_PRINT_COMPLETION:
          serviceImpl.printCompletion((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.printer.PrintStatus>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class PrinterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    PrinterBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.office.printer.PrinterProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Printer");
    }
  }

  private static final class PrinterFileDescriptorSupplier
      extends PrinterBaseDescriptorSupplier {
    PrinterFileDescriptorSupplier() {}
  }

  private static final class PrinterMethodDescriptorSupplier
      extends PrinterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    PrinterMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (PrinterGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new PrinterFileDescriptorSupplier())
              .addMethod(getToggleMethod())
              .addMethod(getPrintMethod())
              .addMethod(getStatusMethod())
              .addMethod(getPrintCompletionMethod())
              .build();
        }
      }
    }
    return result;
  }
}
