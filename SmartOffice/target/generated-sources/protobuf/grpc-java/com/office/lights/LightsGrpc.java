package com.office.lights;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The lights service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.19.0)",
    comments = "Source: lights.proto")
public final class LightsGrpc {

  private LightsGrpc() {}

  public static final String SERVICE_NAME = "lights.Lights";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getToggleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Toggle",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.lights.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getToggleMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.lights.Response> getToggleMethod;
    if ((getToggleMethod = LightsGrpc.getToggleMethod) == null) {
      synchronized (LightsGrpc.class) {
        if ((getToggleMethod = LightsGrpc.getToggleMethod) == null) {
          LightsGrpc.getToggleMethod = getToggleMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.lights.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "lights.Lights", "Toggle"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.lights.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new LightsMethodDescriptorSupplier("Toggle"))
                  .build();
          }
        }
     }
     return getToggleMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getStatusMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Status",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.lights.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getStatusMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.lights.Response> getStatusMethod;
    if ((getStatusMethod = LightsGrpc.getStatusMethod) == null) {
      synchronized (LightsGrpc.class) {
        if ((getStatusMethod = LightsGrpc.getStatusMethod) == null) {
          LightsGrpc.getStatusMethod = getStatusMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.lights.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "lights.Lights", "Status"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.lights.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new LightsMethodDescriptorSupplier("Status"))
                  .build();
          }
        }
     }
     return getStatusMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getBrighterMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Brighter",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.lights.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getBrighterMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.lights.Response> getBrighterMethod;
    if ((getBrighterMethod = LightsGrpc.getBrighterMethod) == null) {
      synchronized (LightsGrpc.class) {
        if ((getBrighterMethod = LightsGrpc.getBrighterMethod) == null) {
          LightsGrpc.getBrighterMethod = getBrighterMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.lights.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "lights.Lights", "Brighter"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.lights.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new LightsMethodDescriptorSupplier("Brighter"))
                  .build();
          }
        }
     }
     return getBrighterMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getDimmerMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Dimmer",
      requestType = com.google.protobuf.Empty.class,
      responseType = com.office.lights.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.google.protobuf.Empty,
      com.office.lights.Response> getDimmerMethod() {
    io.grpc.MethodDescriptor<com.google.protobuf.Empty, com.office.lights.Response> getDimmerMethod;
    if ((getDimmerMethod = LightsGrpc.getDimmerMethod) == null) {
      synchronized (LightsGrpc.class) {
        if ((getDimmerMethod = LightsGrpc.getDimmerMethod) == null) {
          LightsGrpc.getDimmerMethod = getDimmerMethod = 
              io.grpc.MethodDescriptor.<com.google.protobuf.Empty, com.office.lights.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "lights.Lights", "Dimmer"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.office.lights.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new LightsMethodDescriptorSupplier("Dimmer"))
                  .build();
          }
        }
     }
     return getDimmerMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static LightsStub newStub(io.grpc.Channel channel) {
    return new LightsStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static LightsBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new LightsBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static LightsFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new LightsFutureStub(channel);
  }

  /**
   * <pre>
   * The lights service definition.
   * </pre>
   */
  public static abstract class LightsImplBase implements io.grpc.BindableService {

    /**
     */
    public void toggle(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getToggleMethod(), responseObserver);
    }

    /**
     */
    public void status(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getStatusMethod(), responseObserver);
    }

    /**
     */
    public void brighter(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getBrighterMethod(), responseObserver);
    }

    /**
     */
    public void dimmer(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getDimmerMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getToggleMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.lights.Response>(
                  this, METHODID_TOGGLE)))
          .addMethod(
            getStatusMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.lights.Response>(
                  this, METHODID_STATUS)))
          .addMethod(
            getBrighterMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.lights.Response>(
                  this, METHODID_BRIGHTER)))
          .addMethod(
            getDimmerMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.google.protobuf.Empty,
                com.office.lights.Response>(
                  this, METHODID_DIMMER)))
          .build();
    }
  }

  /**
   * <pre>
   * The lights service definition.
   * </pre>
   */
  public static final class LightsStub extends io.grpc.stub.AbstractStub<LightsStub> {
    private LightsStub(io.grpc.Channel channel) {
      super(channel);
    }

    private LightsStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected LightsStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new LightsStub(channel, callOptions);
    }

    /**
     */
    public void toggle(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getToggleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void status(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getStatusMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void brighter(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getBrighterMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void dimmer(com.google.protobuf.Empty request,
        io.grpc.stub.StreamObserver<com.office.lights.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDimmerMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The lights service definition.
   * </pre>
   */
  public static final class LightsBlockingStub extends io.grpc.stub.AbstractStub<LightsBlockingStub> {
    private LightsBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private LightsBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected LightsBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new LightsBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.office.lights.Response toggle(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getToggleMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.office.lights.Response status(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getStatusMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.office.lights.Response brighter(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getBrighterMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.office.lights.Response dimmer(com.google.protobuf.Empty request) {
      return blockingUnaryCall(
          getChannel(), getDimmerMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The lights service definition.
   * </pre>
   */
  public static final class LightsFutureStub extends io.grpc.stub.AbstractStub<LightsFutureStub> {
    private LightsFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private LightsFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected LightsFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new LightsFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.lights.Response> toggle(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getToggleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.lights.Response> status(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getStatusMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.lights.Response> brighter(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getBrighterMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.office.lights.Response> dimmer(
        com.google.protobuf.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getDimmerMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_TOGGLE = 0;
  private static final int METHODID_STATUS = 1;
  private static final int METHODID_BRIGHTER = 2;
  private static final int METHODID_DIMMER = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final LightsImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(LightsImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_TOGGLE:
          serviceImpl.toggle((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.lights.Response>) responseObserver);
          break;
        case METHODID_STATUS:
          serviceImpl.status((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.lights.Response>) responseObserver);
          break;
        case METHODID_BRIGHTER:
          serviceImpl.brighter((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.lights.Response>) responseObserver);
          break;
        case METHODID_DIMMER:
          serviceImpl.dimmer((com.google.protobuf.Empty) request,
              (io.grpc.stub.StreamObserver<com.office.lights.Response>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class LightsBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    LightsBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.office.lights.LightsProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Lights");
    }
  }

  private static final class LightsFileDescriptorSupplier
      extends LightsBaseDescriptorSupplier {
    LightsFileDescriptorSupplier() {}
  }

  private static final class LightsMethodDescriptorSupplier
      extends LightsBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    LightsMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (LightsGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new LightsFileDescriptorSupplier())
              .addMethod(getToggleMethod())
              .addMethod(getStatusMethod())
              .addMethod(getBrighterMethod())
              .addMethod(getDimmerMethod())
              .build();
        }
      }
    }
    return result;
  }
}
